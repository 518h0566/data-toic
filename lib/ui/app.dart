
import 'package:flutter/material.dart';

import 'dart:convert';

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    String jsonArray = '''
    [
    {
        "no" : 1,
        "image" : "1.png",
        "audio" : "1.mp3"
    },
    {
        "no" : 2,
        "image" : "2.png",
        "audio" : "2.mp3"
    },
    {
        "no" : 3,
        "image" : "3.png",
        "audio" : "3.mp3"
    },
    {
        "no" : 4,
        "image" : "4.png",
        "audio" : "4.mp3"
    },
    {
        "no" : 5,
        "image" : "5.png",
        "audio" : "5.mp3"
    },
    {
        "no" : 6,
        "image" : "6.png",
        "audio" : "6.mp3"
    }
]
    ''';
    var dataList = json.decode(jsonArray);

    var appWidget = MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('My Image Viewer - v0.0.10'),),
        body: ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    Text("img number "+dataList[index]["no"].toString()+" :", style: new TextStyle(color: Colors.red),),
                    Image.network("https://thachln.github.io/toeic-data/ets/2016/1/p1/"+dataList[index]["image"]),
                ],
                ),
              ),
            );
          },
        ),
      ),
  );

    return appWidget;
  }
}